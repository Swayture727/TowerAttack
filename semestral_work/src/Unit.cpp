
#include "Unit.h"
#include <string>
#include <tuple>
#include <iostream>

Unit::Unit( size_t x, 
            size_t y, 
            char drawChar, 
            int hp,
            int dmg, 
            int range,
            UnitAttackE * attackEff,
            UnitMovementE * moveEff,
            UnitDefensiveE * defEff,
            size_t pathRestoration)
    :   stats(hp,std::unique_ptr<UnitDefensiveE>(defEff)),
        dmg(dmg),
        range(range),
        sprite(x,y,drawChar),
        move(std::unique_ptr<UnitMovementE>(moveEff)),
        attack(std::unique_ptr<UnitAttackE>(attackEff)),
        pathRestoration(pathRestoration){}
//----------------------------------------------------------------------------------//
Unit::Unit( const Unit & unit )
    :   stats(unit.stats),
        dmg(unit.dmg),
        range(unit.range),
        sprite(unit.sprite),
        move(std::unique_ptr<UnitMovementE>(unit.move->clone())),
        attack(std::unique_ptr<UnitAttackE>(unit.attack->clone())),
        pathRestoration(unit.pathRestoration){
        }
//----------------------------------------------------------------------------------//
Unit & Unit::operator = ( const Unit & unit ) {
    stats = unit.stats;
    dmg = unit.dmg;
    range = unit.range;
    sprite = unit.sprite;
    attack = std::unique_ptr<UnitAttackE>(unit.attack->clone());
    move = std::unique_ptr<UnitMovementE>(unit.move->clone());
    pathRestoration = unit.pathRestoration;
    return *this;
}
//----------------------------------------------------------------------------------//
void Unit::postRoundReset(){
    stats.postRoundReset();
    attack->postRoundReset(dmg);
    move->postRoundReset();
}
//----------------------------------------------------------------------------------//
size_t Unit::getHeight() const{
    return move->getHeight();
}
//----------------------------------------------------------------------------------//
std::pair<size_t,size_t> Unit::walk() {
    std::pair<size_t,size_t> nextPosition = move->walk(stats.getStatus());
    sprite.changePosition(nextPosition.first, nextPosition.second);
    return nextPosition;
}
//----------------------------------------------------------------------------------//
bool Unit::attackTower(std::shared_ptr<Tower> tower){
    std::pair<size_t, size_t> towerCords = tower->getCords();
    size_t towerX = towerCords.first; 
    size_t towerY = towerCords.second; 

    std::pair<size_t, size_t> unitCord = getCords();
    size_t unitX = unitCord.first;
    size_t unitY = unitCord.second;

    if( (unitX > towerX ? unitX - towerX : towerX - unitX) + 
        (unitY > towerY ? unitY - towerY : towerY - unitY) <= range ){
            return tower->takeDamage(dmg);
        }
    return false;
}
//----------------------------------------------------------------------------------//
void Unit::setPath(     std::vector<std::pair<size_t,size_t>>::const_iterator walkingPath,
                        size_t walkingLenght,
                        std::vector<std::pair<size_t,size_t>>::const_iterator flyingPath,
                        size_t flyingLenght){
    std::pair<size_t,size_t> nextPosition = move->setPath(walkingPath,walkingLenght, flyingPath, flyingLenght, pathRestoration);

    sprite.changePosition(nextPosition.first, nextPosition.second);
}
//----------------------------------------------------------------------------------//
std::pair<size_t,size_t> Unit::getCords() const{
    return sprite.getCords();
}
//----------------------------------------------------------------------------------//
std::string Unit::returnSerializedString( ) const{ 
    char drawChar = sprite.drawCharacter;
    std::string buffer;
    return std::string(std::string(1,drawChar) + "," +std::to_string(dmg) + "," + std::to_string(range) + "," 
                        + stats.returnSerializedString() + "," + std::to_string(attack->returnSerializedString()) 
                        + ","  + move->returnSerializedString());
    
}
//----------------------------------------------------------------------------------//
char Unit::getDrawChar() const{
    return sprite.drawCharacter;
}
