
#include "UnitSniperAtt.h"
#include <memory>

bool UnitSniperAtt::attack(std::shared_ptr<Tower> tower, int dmg) {
    if( !target.lock() )
        target = std::weak_ptr<Tower>(tower);
    else if ( target.lock()->getValue() < tower->getValue() ){ 
        target = std::weak_ptr<Tower>(tower);
    }
    return false;
}


bool UnitSniperAtt::postRoundReset( int dmg ){
    if( reloading <= 0 ){
        reloading = 20;
        if( auto tower = target.lock() ){
            target.reset();
            return tower->takeDamage(dmg); 
        }
    }
    else{ 
        reloading -= 1;
    }
    return false;
}

int UnitSniperAtt::returnSerializedString( ) const{
    return 1;
}
