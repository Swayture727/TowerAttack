#pragma once

#include "UnitDefensiveE.h"
class UnitTankDef : public UnitDefensiveE{
    public:
	int takeDmg ( int dmg ) override;
        int returnSerializedString( ) const override;
};
