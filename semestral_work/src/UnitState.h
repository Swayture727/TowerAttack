#pragma once

enum class UnitState { alive, slowed, dead };
