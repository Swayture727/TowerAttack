#pragma once

#include <memory>

#include "GameSprite.h"
#include "UnitAttackE.h"
#include "UnitSniperAtt.h"
#include "UnitDefensiveE.h"
#include "UnitGroundMovementE.h"
#include "UnitFlyingMove.h"
#include "UnitMovementE.h"
#include "UnitStats.h"

class Unit {
    public:
        Unit(   size_t x, 
                size_t y, 
                char drawChar, 
                int hp,
                int dmg, 
                int range,
                UnitAttackE * attackEff,
                UnitMovementE * moveEff,
                UnitDefensiveE * defEff,
                size_t pathRestoration);


        Unit( const Unit & unit );

        Unit & operator = ( const Unit & unit ); 

        void postRoundReset();

        void drawUnit() const;

        bool attackTower(std::shared_ptr<Tower> tower);

        std::pair<size_t,size_t> walk();

        size_t getHeight() const;

        std::pair<size_t,size_t> getCords() const;

        char getDrawChar() const;

        std::string returnSerializedString( ) const;


        void setPath(   std::vector<std::pair<size_t,size_t>>::const_iterator walkingPath,
                        size_t walkingLenght,
                        std::vector<std::pair<size_t,size_t>>::const_iterator flyingPath,
                        size_t flyingLenght);

        UnitStats stats;
    private:
        int dmg;
        size_t range;
        GameSprite sprite;
        std::unique_ptr<UnitMovementE> move;
        std::unique_ptr<UnitAttackE> attack;
        size_t pathRestoration;
};
