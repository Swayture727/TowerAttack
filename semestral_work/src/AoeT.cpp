#include "AoeT.h"

bool AoeT::affect(UnitStats & unit, size_t height) {
    if( fired <= 0 ){
        int dmgOutput = dmg;
        fired = 10;
        return unit.receiveDmg( dmgOutput == 0 ? 1 : dmgOutput ); 
    }
    return false;
}
//----------------------------------------------------------------------------------//
AoeT * AoeT::clone() const{
    return new AoeT(dmg);
}
//----------------------------------------------------------------------------------//
void AoeT::postRoundReset() {
    fired -= 1;
}
//----------------------------------------------------------------------------------//
int AoeT::getSerializedVal() const{
    return 1; 
}
