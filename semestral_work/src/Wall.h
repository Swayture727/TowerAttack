#pragma once

#include <vector>
#include <memory>

#include "GameSprite.h"
#include "Tower.h"


class Wall {
    public:
        Wall( size_t x, size_t y ) 
            :  sprite(x,y, '#'){}
        std::pair<size_t,size_t> getCords() const;
    private:
        GameSprite sprite;
};
