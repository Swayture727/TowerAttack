#pragma once

#include "TowerEffect.h"
#include "UnitStats.h"

/**
 * A TowerEffect that is effective against flying units.
 * @see TowerEffect 
 */
class AntiAirT : public TowerEffect{
    public:
        AntiAirT( int dmg )
            :   TowerEffect(dmg, 3),
                fired(0){};
//----------------------------------------------------------------------------------//
        int getSerializedVal() const override;
//----------------------------------------------------------------------------------//
        bool affect(UnitStats & unit, size_t unitHeight) override;
//----------------------------------------------------------------------------------//
        void postRoundReset() override;
//----------------------------------------------------------------------------------//
        AntiAirT * clone() const override;
    private:
        int fired;
};


