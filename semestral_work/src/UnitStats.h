#pragma once
#include <memory>
#include <string>

#include "UnitDefensiveE.h"
#include "UnitState.h"


class UnitStats{
    public:
        UnitStats( int hp, std::unique_ptr<UnitDefensiveE> takeDamage)
            :   hp(hp),
                currentState(UnitState::alive),
                takeDamage(std::move(takeDamage)) {}
        UnitStats(const UnitStats & stats) 
            :   hp(stats.hp), 
                currentState(stats.currentState) {
                    takeDamage = std::make_unique<UnitDefensiveE>(*stats.takeDamage);
                }
        UnitStats & operator = (const UnitStats & stats);
        void slowUnit();	
        bool receiveDmg (int dmg);	

        bool postRoundReset();

        UnitState getStatus() const;

        std::string returnSerializedString( ) const;
    private:
        int hp;
        UnitState currentState;
        std::unique_ptr<UnitDefensiveE> takeDamage;
};
