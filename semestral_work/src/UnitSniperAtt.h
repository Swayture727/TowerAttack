#pragma once
#include "UnitAttackE.h"
#include <memory>


class UnitSniperAtt : public UnitAttackE{
    public:
        bool attack(std::shared_ptr<Tower> tower, int dmg) override;

        int returnSerializedString( ) const override;
        bool postRoundReset( int dmg ) override;
    private:
        std::weak_ptr<Tower> target;
        int reloading;

};
