#include "GameManager.h"
#include <exception>
#include <iostream>
#include <stdexcept>
#include <unistd.h>

int main(){
    GameManager game;

    if(!isatty(STDOUT_FILENO)) { // muze se hodit otestovat i vstup, STDIN_FILENO
        std::cerr << "Game must run in a terminal!" << std::endl;
        return EXIT_FAILURE;
    }
    try{
        game.run();
    }
    catch(std::range_error & ex){
        std::cout << "Something went wrong: \"" << ex.what() << "\"" << std::endl;
    }
    return EXIT_SUCCESS;
}
