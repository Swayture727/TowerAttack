#pragma once

#include "Tower.h"
#include "UnitStats.h"

class UnitAttackE{
    public:
        UnitAttackE()
            : attacked(0){}
        virtual bool attack(std::shared_ptr<Tower> tower, int dmg) ;
        virtual bool postRoundReset( int dmg );
        virtual int returnSerializedString( ) const;
        virtual ~UnitAttackE() = default;
        virtual UnitAttackE * clone() const;
    private:
        int attacked;

};
