#pragma once

#include <string>


class UnitDefensiveE{
    public:
        UnitDefensiveE() = default;
        virtual int takeDmg(int dmg);
        UnitDefensiveE * clone() const;

        virtual int returnSerializedString( ) const;
        virtual ~UnitDefensiveE() = default;
    private:
};
