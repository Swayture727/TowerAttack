#include "UnitGroundMovementE.h"
#include <string>


UnitGroundMovementE * UnitGroundMovementE::clone() const{
    return new UnitGroundMovementE(*this);
}

std::pair<size_t,size_t> UnitGroundMovementE::walk( const UnitState & state ) {
    if( walkingPath ){
        std::pair<size_t,size_t> goingTo = **walkingPath;
        if( movePenalty <= 0 ){
            if( state == UnitState::slowed )
                movePenalty = 3;
            if( lengthWalked < walkingLenght ){
                lengthWalked++;
                *(*(walkingPath))++;
                goingTo = **(walkingPath); 
                return goingTo;
            }
        }
        else{ 
            movePenalty -= 1;
        }
        return goingTo;
    }
    return std::pair<size_t,size_t>{-1,-1};
}

void UnitGroundMovementE::postRoundReset(){

}

std::pair<size_t,size_t> UnitGroundMovementE::setPath(  std::vector<std::pair<size_t,size_t>>::const_iterator walkingPathIn,
                                                        size_t walkingLenghtIn,
                                                        std::vector<std::pair<size_t,size_t>>::const_iterator flyingPathIn,
                                                        size_t flyingLenghtIn, size_t pathRestoration ){
    if(pathRestoration >= walkingLenghtIn){
        lengthWalked = walkingLenghtIn-2;
        walkingLenght = walkingLenghtIn;
        walkingPath = walkingPathIn + (walkingLenghtIn-2);
    }
    else{
        lengthWalked = pathRestoration ;
        walkingLenght = walkingLenghtIn;
        walkingPath = walkingPathIn + lengthWalked;
    }
    return **walkingPath;
}

std::string UnitGroundMovementE::returnSerializedString( ) const{
    return std::to_string(0) + "," + std::to_string(lengthWalked);
}


