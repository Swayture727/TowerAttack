#pragma once

#include "TowerEffect.h"

/**
 * A basic TowerEffect that attacks one tower.
 * @see TowerEffect
 */
class BasicAttackT : public TowerEffect{
    public:
        BasicAttackT( int dmg )
            :   TowerEffect(dmg, 1),
                fired(0) {}
//----------------------------------------------------------------------------------//
        bool affect(UnitStats & unit, size_t unitHeight) override;
//----------------------------------------------------------------------------------//
        int getSerializedVal() const override;
//----------------------------------------------------------------------------------//
        void postRoundReset() override;
//----------------------------------------------------------------------------------//
        BasicAttackT * clone() const override;
    private:
        int fired;
};
