#include <algorithm>
#include <array>
#include <functional>
#include <stdexcept>

#include "GameMap.h"



//----------------------------------------------------------------------------------//


void GameMap::prepareMap(){
    loadPaths();
    std::function<bool(const Wall & a, const Wall & b)> comparator = [&](const Wall & a, const Wall & b) -> bool
                                                            { return manhattanDistanceFromExit(a.getCords()) < manhattanDistanceFromExit(b.getCords());};
    std::sort(walls.begin(), walls.end(),comparator );
}

void GameMap::loadPaths(){
    findPaths(true);
    findPaths(false);
}

//----------------------------------------------------------------------------------//
void GameMap::drawMap() const {
    std::vector<std::vector<char>> outputBuffer(height,std::vector<char>(width,' '));
    std::pair<size_t,size_t> curCords;
    for( auto & wall : walls ){
        curCords = wall.getCords();
        outputBuffer[curCords.second][curCords.first] = '#';
    }
    for( auto & tower : towers ){
        curCords = tower->getCords();
        outputBuffer[curCords.second][curCords.first] = tower->getDrawChar();
    }
    for(size_t i = 0; i < units.size(); i++){
        curCords = units[i].getCords();
        outputBuffer[curCords.second][curCords.first] = units[i].getDrawChar();
    }
    curCords = exit.getCords();
    outputBuffer[curCords.second][curCords.first] = '<';

    curCords = entrance.getCords();
    outputBuffer[curCords.second][curCords.first] = '>';
    for( auto & line : outputBuffer ){
        for( char & c : line)
            std::cout << c;
        std::cout << std::endl;
    }
}

//----------------------------------------------------------------------------------//

std::tuple<size_t,size_t, bool> GameMap::getNextTowerPlacement() {

    if( nextTowerWallIndex >= walls.size() )
        nextTowerWallIndex = 0;
    std::pair<size_t,size_t> nextCords = walls[nextTowerWallIndex].getCords();
    nextTowerWallIndex++;
    return {nextCords.first, nextCords.second, true}; 
}

//----------------------------------------------------------------------------------//

size_t GameMap::manhattanDistanceFromExit(std::pair<size_t, size_t> a) const{ 
    std::pair<size_t, size_t> b = exit.getCords();
    return( (a.first > b.first ? a.first - b.first : b.first - a.first) + 
            (a.second > b.second ? a.second - b.second : b.second - a.second)); 
}
//----------------------------------------------------------------------------------//
void GameMap::reconstructPath(std::vector<int>& tileParents, size_t index, bool canFly) {
    std::pair<size_t,size_t> entrancePair = entrance.getCords();
    int entranceIndex = entrancePair.first + entrancePair.second * width ; 

    std::pair<size_t,size_t> exitPair = exit.getCords();
    if( canFly ){
        flyingPath.push_back(exitPair);
        while( tileParents[index] != entranceIndex ){
            flyingPath.push_back({tileParents[index]%width, tileParents[index]/width});
            index = tileParents[index];
        }
        flyingPath.push_back(entrancePair);
        std::reverse(flyingPath.begin(),flyingPath.end());
    }
    else{
        walkingPath.push_back(exitPair);
        while( tileParents[index] != entranceIndex ){
            walkingPath.push_back({tileParents[index]%width, tileParents[index]/width});
            index = tileParents[index];
        }
        walkingPath.push_back(entrancePair);
        std::reverse(walkingPath.begin(),walkingPath.end());
    }


}


//----------------------------------------------------------------------------------//
void GameMap::expand( size_t index, std::queue<int>& tileIndexes, 
                            std::vector<int>& tileParents, bool canFly) {
    size_t directions[4] = {index+1,index+width, index-1, index-width};
    size_t size = gameBoard.size();
    for( int i = 0; i < 4; ++i ){
        bool outOfBounds = directions[i] >= size;
        bool notOnTheSameLine = (i == 0 || i == 2) && index / (width) != directions[i] / (width) ? true : false;
        if( outOfBounds ||
            notOnTheSameLine){
                continue;
        }
        bool notExpandable = !((gameBoard[directions[i]] || canFly) 
                                && tileParents[directions[i]] == -1);
        if( notExpandable )
            continue;
        tileIndexes.push(directions[i]);
        tileParents[directions[i]] = index;
    }
}
//----------------------------------------------------------------------------------//
void GameMap::findPaths(bool canFly) {
    std::pair<size_t,size_t> entrancePair = entrance.getCords();
    size_t entranceIndex = entrancePair.first + entrancePair.second * (width) ; 

    std::pair<size_t,size_t> exitPair = exit.getCords();
    size_t exitIndex = exitPair.first + exitPair.second * (width) ; 

    std::queue<int> tileIndexes;	
    std::vector<int> tileParents = std::vector<int>(gameBoard.size(),-1);
    tileIndexes.push(entranceIndex);
    while( tileIndexes.size() ){
        size_t currentIndex = tileIndexes.front();
        tileIndexes.pop();
        if( currentIndex == exitIndex ){
            reconstructPath(tileParents,currentIndex, canFly);
        }
        expand(currentIndex,tileIndexes,tileParents, canFly);
    }
    //TODO: another place where the throw msg should be in the catch statement 
    if( !canFly && walkingPath.size() == 0 ){
        throw std::logic_error("Can't get to the exit!");
    }
} 




