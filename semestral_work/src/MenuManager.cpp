
#include <exception>
#include <limits>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <termio.h>
#include <unistd.h>

#include "MenuManager.h"
#include "MenuState.h"


MenuManager::MenuManager()
    :   state(MenuState::mainMenu){
            menus.emplace_back(new Menu("Main menu", {
                {"Start game", [&](){state = MenuState::startGame; activeMenu = menus.begin()+1;}},
                {"Load game", [&](){state = MenuState::loadGame;}},
                {"Quit Game", [&](){state = MenuState::quit;}}
            }));
            menus.emplace_back(new Menu("Choose action: ", {
                {"Save game", [&](){state = MenuState::saveGame;}},
                {"Go to main menu", [&](){state = MenuState::mainMenu;activeMenu = menus.begin();}},
                {"Quit Game", [&](){state = MenuState::quit;}}})); activeMenu = menus.begin();

        };	


void MenuManager::displayActiveMenu() {
    if( (*activeMenu)->isEmpty() ){
        //TODO change exceptions
        throw std::logic_error("The first menu must have at least one line to display!");
    }
    else if ( state != MenuState::quit ){
        (*activeMenu)->listCommands();
        (*activeMenu)->chooseAndExecuteCommand();
    }
}

//----------------------------------------------------------------------------------//
void MenuManager::setupTerminal() const{
    clear();
    moveCursourto();
    hideCursour();
}

//----------------------------------------------------------------------------------//
void MenuManager::clear() const{
    std::cout << "\033[2J";
}

//----------------------------------------------------------------------------------//
void MenuManager::moveCursourto(int x, int y) const{
    std::cout << "\033[" << y << ";" << x << "H";
}


//----------------------------------------------------------------------------------//
void MenuManager::hideCursour() const{
    std::cout << "\033[?25l";
}

//----------------------------------------------------------------------------------//
void MenuManager::showCursour() const{
    std::cout << "\033[?25h";
}

//----------------------------------------------------------------------------------//
bool MenuManager::shouldRun() const {
    return state != MenuState::quit;
}
//----------------------------------------------------------------------------------//
bool MenuManager::shouldSave() {
    bool val = state == MenuState::saveGame;
    if( val )
        state = MenuState::startGame;
    return val;
}
//----------------------------------------------------------------------------------//
bool MenuManager::shouldLoad() {
    bool val = state == MenuState::loadGame;
    if( val )
        state = MenuState::mainMenu;
    return val;
}

//----------------------------------------------------------------------------------//
bool MenuManager::shouldShowMainMenu() const {
    return state == MenuState::mainMenu || 
           state == MenuState::saveGame || 
           state == MenuState::loadGame;
}

//----------------------------------------------------------------------------------//
bool MenuManager::shouldBeIngame() const {
    return state == MenuState::startGame;
}

//----------------------------------------------------------------------------------//
void MenuManager::returnToNormal() const{
    clear();
    showCursour();
    moveCursourto();
}

//----------------------------------------------------------------------------------//
void MenuManager::setupPoolingInput() const{
    termios term;
    tcgetattr(STDIN_FILENO, &term);

    term.c_lflag &= ~(ECHO | ICANON);
    term.c_cc[VMIN]  = 0; 
    term.c_cc[VTIME] = 0;
    tcsetattr(STDIN_FILENO, TCSANOW, &term);
}

//----------------------------------------------------------------------------------//
