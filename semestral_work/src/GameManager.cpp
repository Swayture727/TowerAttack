#include <memory>
#include <ostream>
#include <string>
#include <unistd.h>
#include <chrono>
#include <fstream>

#include "GameManager.h"


GameManager::GameManager() 
            :   state(activeGame),
                score(0), 
                baseHP(1500), 
                numberOfRounds(30), 
                evenTick(true),
                tickCounter(0){
                    gameMap = configManager.loadMap();
                    gameMap.drawMap();
                    std::cout << "This is how the loaded map looks, press enter to continue" << std::endl;
                    auto units = configManager.loadUnitSettings(gameMap.entrance.getCords());
                    for( auto & unit : *units ){
                        availableUnits.emplace_back(Unit(unit));
                    }
                    auto towers = configManager.loadTowerSettings();
                    for( auto & tower : *towers ){
                        availableTowers.emplace_back(*tower);
                    }
                    unitsRemaining = availableUnits.size() * 4 > (size_t)baseHP ? availableUnits.size() * 4 : baseHP * 1.5  ;
                    lastSpawnedUnit = availableUnits.begin();
                    lastSpawnedTower = availableTowers.begin();
                    menu.setupPoolingInput();
                };
//----------------------------------------------------------------------------------//
void GameManager::run(){
    while( menu.shouldRun() ){
        auto start = std::chrono::steady_clock::now();
        menu.setupTerminal();
        menu.displayActiveMenu();
        if( menu.shouldLoad() ){
            loadGame();
        }
        state = menu.shouldRun() ? GameState::activeGame : GameState::quitGame;
        std::chrono::duration<double, std::micro> diff =  start - std::chrono::steady_clock::now();
        usleep(diff.count() > 100000 ? 0 : diff.count() + 100000);
        if( menu.shouldBeIngame() && state != GameState::quitGame )
            mainGameLoop();
    }
    menu.returnToNormal();
}
//----------------------------------------------------------------------------------//
void GameManager::mainGameLoop(){
    while( menu.shouldBeIngame() && state == activeGame ){	
        auto start = std::chrono::steady_clock::now();
        menu.setupTerminal();
        gameMap.drawMap();
        displayStats();
        menu.displayActiveMenu();
        if( menu.shouldSave() ){
            saveGame();
        }
        turnActions(); 
        std::chrono::duration<double, std::micro> diff =  start - std::chrono::steady_clock::now();
        usleep(diff.count() > 100000 ? 0 : diff.count() + 100000);
    }

    while( state != activeGame && menu.shouldBeIngame() ){
        auto start = std::chrono::steady_clock::now();
        menu.setupTerminal();
        displayStats();
        std::string msg = state == wonGame ? "You've won!" : "You've lost :(";
        std::cout << msg << std::endl;
        menu.displayActiveMenu();
        std::chrono::duration<double, std::micro> diff =  start - std::chrono::steady_clock::now();
        usleep(diff.count() > 1000000 ? 0 : diff.count() + 100000);
    }
    menu.setupTerminal();
}
//----------------------------------------------------------------------------------//
void GameManager::turnActions(){

    towerAction();

    unitAction();

    spawnUnits();

    placeTower();

    postRoundReset();

    evenTick = !evenTick;
    tickCounter += 1;
}
//----------------------------------------------------------------------------------//
void GameManager::spawnUnits(){
    if( evenTick ){
        if( unitsRemaining == 0  ){
            if( gameMap.units.size() == 0)
                state = GameState::lostGame;
        }
        else{
            gameMap.units.emplace_back(*lastSpawnedUnit);
            gameMap.units.back().setPath(   gameMap.walkingPath.begin(), gameMap.walkingPath.size(), 
                                            gameMap.flyingPath.begin(), gameMap.flyingPath.size());
            unitsRemaining -= 1;
            ++lastSpawnedUnit;
            if( lastSpawnedUnit == availableUnits.end() )
                lastSpawnedUnit = availableUnits.begin();
        }
    }
}
//----------------------------------------------------------------------------------//
void GameManager::postRoundReset(){
    for( auto & unit : gameMap.units ){
        unit.postRoundReset();
    }
    for( auto & tower : gameMap.towers){
        tower->postRoundReset();
    }
}
//----------------------------------------------------------------------------------//
void GameManager::placeTower(){
    if( tickCounter == 32 ){
        tickCounter = 0; 
        std::tuple<size_t, size_t, bool> nextCords = gameMap.getNextTowerPlacement(); 
        if( std::get<2>(nextCords) ){
            lastSpawnedTower++;
            if( lastSpawnedTower == availableTowers.end() )
                lastSpawnedTower = availableTowers.begin();
            std::shared_ptr<Tower> tower = std::make_shared<Tower>(Tower((*lastSpawnedTower)));
            tower->buildTowerOn({std::get<0>(nextCords), std::get<1>(nextCords)});
            gameMap.towers.push_back(std::move(tower));
        }

    }
}
//----------------------------------------------------------------------------------//
void GameManager::unitAction(){
    auto exitCords = gameMap.exit.getCords();
    for( size_t i = 0; i < gameMap.units.size(); i++ ){
        Unit & unit = gameMap.units[i];
        for( size_t j = 0; j < gameMap.towers.size(); j++ ){
            auto tower = gameMap.towers[j];
            if( unit.attackTower(tower) ){
                score += 1;
                gameMap.towers.erase(gameMap.towers.begin() + j);
            }
        }
        if( unit.walk() == exitCords ){
            gameMap.units.erase(gameMap.units.begin() + i); 
            baseHP -= 1;
            if( baseHP == 0 ){
                state = GameState::wonGame;
            }
       } 
    }
}
//----------------------------------------------------------------------------------//
void GameManager::towerAction(){
    for( auto & tower : gameMap.towers ){
        for( size_t i = 0; i < gameMap.units.size(); i++){
            auto & unit = gameMap.units[i];
            std::pair<size_t, size_t> cords = unit.getCords();
            if (tower->affect(unit.stats, {cords.first, cords.second, unit.getHeight()}))
                gameMap.units.erase(gameMap.units.begin() + i);
        }
    }
    
}
//----------------------------------------------------------------------------------//
void GameManager::saveGame(){

    std::ofstream out = std::ofstream("examples/configFiles/AvUSaveFile.txt", std::ios::trunc);
    for( auto & unit : availableUnits ){
        out << unit.returnSerializedString() << std::endl;
    }
    out.close();
    out = std::ofstream("examples/configFiles/AvTSaveFile.txt", std::ios::trunc);
    for( auto & tower : availableTowers ){
        out << tower.returnSerializedString()<< std::endl;
    }
    out.close();

    out = std::ofstream("examples/configFiles/AcUSaveFile.txt", std::ios::trunc);
    for( auto & unit : gameMap.units ){
        out << unit.returnSerializedString()  << std::endl;
    }
    out.close();
    out = std::ofstream("examples/configFiles/AcTSaveFile.txt", std::ios::trunc);
    for( auto & tower : gameMap.towers ){
        out << tower->returnSerializedString() << std::endl; 
    }
    out.close();

    out = std::ofstream("examples/configFiles/SavedMap.txt", std::ios::trunc);
    std::vector<std::vector<char>> outputBuffer(gameMap.height,std::vector<char>(gameMap.width,' '));
    std::pair<size_t,size_t> curCords;
    for( auto & wall : gameMap.walls ){
        curCords = wall.getCords();
        outputBuffer[curCords.second][curCords.first] = '#';
    }

    curCords = gameMap.exit.getCords();
    outputBuffer[curCords.second][curCords.first] = '<';

    curCords = gameMap.entrance.getCords();
    outputBuffer[curCords.second][curCords.first] = '>';
    for( auto & line : outputBuffer ){
        for( char & c : line)
            out << c;
        out << std::endl;
    }

}
//----------------------------------------------------------------------------------//
void GameManager::loadGame(){
    auto potencialMap = configManager.getSavedMap();
    if( !potencialMap )
        return;
    auto potencialAvaiUnits = configManager.loadUnitSettings(potencialMap->entrance.getCords(), "AvUSaveFile.txt" );
    auto potencialAvaiTowers = configManager.loadTowerSettings("AvTSaveFile.txt" );

    auto potencialActiUnits = configManager.loadUnitSettings(potencialMap->entrance.getCords(), "AcUSaveFile.txt" );
    
    auto potencialActiTowers = configManager.loadTowerSettings("AcTSaveFile.txt", true );

    gameMap = *potencialMap;

    gameMap.units.clear();
    for( auto & unit : *potencialActiUnits ){
        gameMap.units.emplace_back(Unit(unit));
        gameMap.units.back().setPath(   gameMap.walkingPath.begin(),gameMap.walkingPath.size(), 
                                            gameMap.flyingPath.begin(), gameMap.flyingPath.size());
    }
    gameMap.towers.clear();
    for( auto & tower : *potencialActiTowers ){
        gameMap.towers.emplace_back(tower);
    }

    availableUnits.clear();
    availableTowers.clear();

    for( auto & unit : *potencialAvaiUnits){
        availableUnits.emplace_back(Unit(unit));
    }

    for( auto & tower : *potencialAvaiTowers ){
        availableTowers.emplace_back(*tower);
    }

    lastSpawnedUnit = availableUnits.begin();
    lastSpawnedTower = availableTowers.begin();
}
//----------------------------------------------------------------------------------//
void GameManager::displayStats(){
    std::cout   << "Enemy base has: " << baseHP << " HP" << std::endl 
                << "Your score is: " << score << std::endl\
                << "Units remaining " << unitsRemaining << std::endl;
}
