
#include "GameSprite.h"

/**
 * Stores map entrance.
 */
class Entrance{
    public:
        Entrance(size_t y = 1)
            : sprite(0, y, '>'){}
        std::pair<size_t,size_t> getCords() const;
        size_t getIndex() const;
        
    private:
        GameSprite sprite;

};
