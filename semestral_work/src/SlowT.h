#pragma once

#include "TowerEffect.h"
#include "UnitStats.h"

class SlowT : public TowerEffect{
    public:
        SlowT()
            : TowerEffect(0, 10){}
        bool affect(UnitStats & unit, size_t height) override;
        int getSerializedVal() const override;
        void postRoundReset() override;
        SlowT * clone() const override;
    private:
}; 
