#include "AntiAirT.h"

bool AntiAirT::affect(UnitStats & unit, size_t height ) {
    if( fired <= 0 ){

        fired = 2;
        if( height == 1 ){
           return unit.receiveDmg( dmg * 2 ); 
        } 
        else{
           return unit.receiveDmg( dmg / 4 ); 
        }
    }
    return false;
} 
//----------------------------------------------------------------------------------//
void AntiAirT::postRoundReset() {
    fired -= 1;
}
//----------------------------------------------------------------------------------//
AntiAirT * AntiAirT::clone() const{
    return new AntiAirT(dmg);
}
//----------------------------------------------------------------------------------//
int AntiAirT::getSerializedVal() const{
    return 3; 
}
