#pragma once

#include <optional>
#include <string>

#include "Tower.h"
#include "BasicAttackT.h"
#include "AoeT.h"
#include "SlowT.h"


class TowerFactory{
    public:
        Tower operator() (  char c,
                            int hp,
                            int dmg,
                            int range,
                            std::vector<size_t> effects) const;

        Tower createActiveTower( int x, 
                                 int y, 
                                 char c, 
                                 int hp, 
                                 int dmg, 
                                 int range, 
                                 std::vector<size_t> effects) const;

    private:


};
