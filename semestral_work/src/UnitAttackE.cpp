

#include "UnitAttackE.h"

bool UnitAttackE::attack(std::shared_ptr<Tower> tower, int dmg) {
    if( attacked <= 0 ){
        attacked = 3;
        return tower->takeDamage(dmg);
    }
    return false;
}

bool UnitAttackE::postRoundReset( int dmg ) {
    attacked -= 1;
    return false;
}

UnitAttackE * UnitAttackE::clone() const{
    return new UnitAttackE();
}

int UnitAttackE::returnSerializedString( ) const{
    return 0;
}
