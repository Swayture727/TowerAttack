#include <unistd.h>
#include <termios.h>
#include <iostream>
#include "Menu.h"

void Menu::listCommands() const {
    std::cout << "\033[33m";
    std::cout << menuTitle << std::endl;
    for(size_t i = 0; i < menuLines.size(); i++) {
        std::cout << "[" << (i+1) << "] " << menuLines[i].title << std::endl;
    }

    std::cout << "\033[34m";
    std::cout << "Enter the desired commands number: " << "\033[m" << std::flush;
}

void Menu::chooseAndExecuteCommand() const{
    char c = 'z';
    read(fileno(stdin), &c, 1); 	
    if( c > '0' && c < '1' + sizeOfMenu ){
        menuLines[c - '1'].function();
    }
}

bool Menu::isEmpty() const{
    return menuLines.size() == 0;
}

void Menu::display() const{
    listCommands();
    chooseAndExecuteCommand();
}
