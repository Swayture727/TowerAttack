#pragma once

#include <functional>
#include <map>

#include "Wall.h"

/**
 * Factory for gametiles - based on input char it creates the right tile.
 */

class GameTileMaker{
    public:
        /** 
         * Creates a gametile if the given char is valid.
         * @exception std::invalid_argument Given char is not a valid gametile.
         */
        bool createGameTile(char identifier);
        GameTileMaker() 
            :   possibleTiles({
                    {'#',[]() -> bool{return false;}},
                    {' ',[]() -> bool{return true;}},
                    {'>',[]() -> bool{return true;}},
                    {'<',[]() -> bool{return true;}}
                }) {}

    private:
        std::map<char, std::function<bool()>> possibleTiles;
};
