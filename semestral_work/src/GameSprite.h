#pragma once
#include <cstdio>
#include <utility>
/**
 * A struct to manage and encapsulate drawable part of:  gameTiles, Units and towers.
 */

class GameSprite{
    public:
        /**
         * Displays itself to std::cout.
         */
        void drawYourself() const;
        GameSprite(size_t x, size_t y, char c)
            :   drawCharacter(c),
                x(x),
                y(y){}
        GameSprite(const GameSprite & sprite)
            :   drawCharacter(sprite.drawCharacter),
                x(sprite.x),
                y(sprite.y){}
        GameSprite & operator= (const GameSprite & sprite);
        std::pair<size_t, size_t> getCords() const;

        void changePosition(size_t x, size_t y);

        char drawCharacter;
    private:
        size_t x;
        size_t y;

        /**
         * A character that will be drawn ingame.
         */
};


