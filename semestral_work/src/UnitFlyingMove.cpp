
#include "UnitFlyingMove.h"
#include <string>
#include <utility>


UnitFlyingMove * UnitFlyingMove::clone() const{
    return new UnitFlyingMove(*this);
}

std::pair<size_t,size_t> UnitFlyingMove::walk( const UnitState & state ){ 
    if( flyingPath ){
        std::pair<size_t,size_t> goingTo = **flyingPath;
        if( lengthWalked < flyingLength ){
            lengthWalked++;
            *(*(flyingPath))++;
            goingTo = **(flyingPath); 
            return goingTo;
        }
        return goingTo;
    }
    return std::pair<size_t,size_t>{-1,-1};
}

void UnitFlyingMove::postRoundReset(){
}

std::pair<size_t,size_t> UnitFlyingMove::setPath(   std::vector<std::pair<size_t,size_t>>::const_iterator walkingPathIn,
                                                    size_t walkingLenghtIn,
                                                    std::vector<std::pair<size_t,size_t>>::const_iterator flyingPathIn,
                                                    size_t flyingLenghtIn, size_t pathRestoration ){
    if(pathRestoration >= walkingLenghtIn){
        lengthWalked = flyingLenghtIn-2;
        flyingLength = flyingLenghtIn;
        flyingPath = flyingPathIn + (flyingLenghtIn-2);
    }
    else{
        lengthWalked = pathRestoration ;
        flyingLength = flyingLenghtIn;
        flyingPath = flyingPathIn + lengthWalked;
    }
    return **flyingPath;

} 

std::string UnitFlyingMove::returnSerializedString() const{
    return std::to_string(1) + "," + std::to_string(lengthWalked);
}
