#include <iostream>

#include "GameSprite.h"


std::pair<size_t, size_t> GameSprite::getCords() const{
    return std::make_pair(x, y);
}

void GameSprite::changePosition(size_t xIn, size_t yIn){
    x = xIn;
    y = yIn;
}

GameSprite & GameSprite::operator=(const GameSprite & sprite){
    drawCharacter = sprite.drawCharacter;
    x = sprite.x;
    y = sprite.y;
    return *this;
}
