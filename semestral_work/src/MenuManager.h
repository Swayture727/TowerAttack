#pragma once

#include <memory>
#include <vector>

#include "Menu.h"
#include "MenuState.h"

class MenuManager{
    public:
        MenuManager();

//----------------------------------------------------------------------------------//
        void displayActiveMenu();

//----------------------------------------------------------------------------------//
        /**
         * Clears screen, hides and moves curour to the top right.
         */

        void setupTerminal() const;

//----------------------------------------------------------------------------------//
        /**
         * Clears whole screen display. 
         *
         */
  
        void clear() const;

//----------------------------------------------------------------------------------//
        /**
         * Moves cursour to the given x and y cords.
         *
         */
  
        void moveCursourto(int x = 1, int y = 1) const;
		
//----------------------------------------------------------------------------------//
        /**
         * Hides cursour. 
         */
 
        void hideCursour() const;
		 
//----------------------------------------------------------------------------------//
        /**
         * Makes cursour visible.
         */
   
        void showCursour() const;
    
//----------------------------------------------------------------------------------//
        /**
         * Sets terminal up to be used in a pooling way.
         * This way of input gets used while ingame in order to be able to get input
         * while rendering game.
         */ 

        void setupPoolingInput() const;

//----------------------------------------------------------------------------------// 
        /**
         * Sets basic terminal settings back.
         */

        void returnToNormal() const;
		
//----------------------------------------------------------------------------------//

        bool shouldRun() const;
		
//----------------------------------------------------------------------------------//

        bool shouldBeIngame() const;

//----------------------------------------------------------------------------------//

        bool shouldShowMainMenu() const;

        bool shouldLoad() ;
        
        bool shouldSave() ;
        
    private:
        std::vector<std::unique_ptr<Menu>> menus;
        MenuState state;
        std::vector<std::unique_ptr<Menu>>::const_iterator activeMenu;

};



