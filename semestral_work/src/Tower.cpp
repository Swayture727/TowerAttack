
#include <math.h>
#include <string>
#include "Tower.h"


Tower::Tower(   size_t x, 
                size_t y, 
                char c, 
                int hp, 
                int dmg,
                int range,
                std::vector<std::unique_ptr<TowerEffect>> & effectsIn)
                :   hp(hp),
                    dmg(dmg),
                    range(range),
                    sprite(x,y,c),
                    towerValue(0){ 
                        for( auto & effect : effectsIn ){
                            effects.emplace_back(effect->clone());
                        }
                    }

Tower::Tower(const Tower & tower)
    :   hp(tower.hp),
        dmg(tower.dmg),
        range(tower.range),
        sprite(tower.sprite),
        towerValue(tower.towerValue){
            effects.clear();
            for( auto & effect : tower.effects ){
                effects.emplace_back(effect->clone());
            }
        }
Tower & Tower::operator= ( const Tower & tower ){
    hp = tower.hp;
    dmg = tower.dmg,
    range = tower.range;
    sprite = tower.sprite;
    towerValue = tower.towerValue;
    effects.clear();
    for( auto & effect : tower.effects ){
       effects.emplace_back(effect->clone()); 
    }
    return *this;
}


std::string Tower::returnSerializedString() const{ 
    char drawChar = sprite.drawCharacter;
    std::string buffer;
    for( auto & effect : effects ){
        buffer += std::to_string((effect->getSerializedVal())) + ",";
    }
    buffer.erase(buffer.end() -1);
    return std::string( std::string(1,drawChar)  +","+ std::to_string(sprite.getCords().first) +"," + std::to_string(sprite.getCords().second) + "," + 
                        std::to_string(hp) + "," + std::to_string(dmg) + "," + std::to_string(range) + "," + buffer);
}


bool Tower::takeDamage( int dmg ){
    hp -= dmg;
    if( hp <= 0 )
        return true;
    return false;
}

bool Tower::affect(UnitStats & stats, std::tuple<size_t,size_t,size_t> unitCords) const{
    std::pair<size_t,size_t> towerCords = sprite.getCords();

    size_t unitX = std::get<0>(unitCords);
    size_t unitY = std::get<1>(unitCords);
    size_t unitHeight = std::get<2>(unitCords);

    size_t towerX = towerCords.first;
    size_t towerY = towerCords.second;
    if( (unitX > towerX ? unitX - towerX : towerX - unitX) + 
        (unitY > towerY ? unitY - towerY : towerY - unitY) <= range ){
        for( auto & effect : effects ){
           if( effect->affect(stats, unitHeight)) 
               return true; 
        }
        
    }
    return false;
}

void Tower::postRoundReset(){
    for( auto & effect : effects ){
        effect->postRoundReset();
    }
}

void Tower::buildTowerOn( std::pair<size_t, size_t> cords){
    if( sprite.getCords() == std::pair<size_t,size_t>{0,0} )
        sprite.changePosition(cords.first, cords.second);
}

int Tower::getValue() const{
    return towerValue;
}

char Tower::getDrawChar() const{
    return sprite.drawCharacter;
}

std::pair<size_t,size_t> Tower::getCords() const{
    return sprite.getCords();
}
