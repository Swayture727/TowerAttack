#pragma once

#include <curses.h>
#include <iostream>
#include <functional>
#include <vector>
#include <string>
#include "MenuState.h"

class Menu{
    struct MenuLine{
        std::string title;
        std::function<void()> function;
        };

    public:
        Menu(std::string menuTitle,std::vector<MenuLine> menuLines) 
	    :   menuTitle(menuTitle), 
                menuLines(menuLines),
                sizeOfMenu(menuLines.size()){}
	 	
        void listCommands() const; 
   	
        void chooseAndExecuteCommand() const;

        void display() const;

        bool isEmpty() const;
    private:
        std::string menuTitle;
        std::vector<MenuLine> menuLines;
        int sizeOfMenu;
};
