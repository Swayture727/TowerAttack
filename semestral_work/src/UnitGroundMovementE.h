#pragma once

#include <vector>
#include <optional>
#include "UnitMovementE.h"



class UnitGroundMovementE : public UnitMovementE{
    public:
        UnitGroundMovementE()
            : UnitMovementE(0),
              walkingLenght(0){};
        UnitGroundMovementE(const UnitGroundMovementE & groundMove) 
            :   walkingPath(groundMove.walkingPath),
                walkingLenght(groundMove.walkingLenght){
                    height = 0;
                }
        std::pair<size_t,size_t> walk( const UnitState & state ) override;
        UnitGroundMovementE * clone() const override;

        std::string returnSerializedString( ) const override;
        void postRoundReset() override;

        std::pair<size_t,size_t> setPath(   std::vector<std::pair<size_t,size_t>>::const_iterator walkingPathIn,
                                            size_t walkingLenghtIn,
                                            std::vector<std::pair<size_t,size_t>>::const_iterator flyingPathIn,
                                            size_t flyingLenghtIn, size_t pathRestoration) override;
    private:
        std::optional<std::vector<std::pair<size_t,size_t>>::const_iterator> walkingPath;
        size_t walkingLenght;

};
