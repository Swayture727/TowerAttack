#pragma once

#include "UnitStats.h"

class TowerEffect{
    public:
        TowerEffect( int dmg, int value )
            :   dmg(dmg),
                value(value){}
        virtual bool affect(UnitStats & unit, size_t unitHeight) = 0 ;
        virtual void postRoundReset() = 0;
        virtual int getSerializedVal() const = 0;
        TowerEffect() = default;
        virtual TowerEffect * clone() const = 0;
        virtual ~TowerEffect() = default;
        int getValue() const{
            return value;
        };
    protected:
        int dmg;
        int value;
};
