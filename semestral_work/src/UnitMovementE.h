#pragma once

#include <utility>
#include <cstdio>
#include <vector>
#include <string>

#include "UnitState.h"

class UnitMovementE{
    public:
        UnitMovementE( size_t height = 0, size_t lengthWalked = 0 )
            :   height(height),
                lengthWalked(lengthWalked),
                movePenalty(0){}
        virtual std::pair<size_t,size_t> setPath(   std::vector<std::pair<size_t,size_t>>::const_iterator walkingPath,
                                size_t walkingLenght,
                                std::vector<std::pair<size_t,size_t>>::const_iterator flyingPath,
                                size_t flyingLenght, size_t pathRestoration = 0 ) = 0;
        virtual ~UnitMovementE() = default;
        virtual std::pair<size_t,size_t> walk( const UnitState & unitState ) = 0 ;
        virtual UnitMovementE * clone() const = 0 ;
        virtual void postRoundReset() = 0;
        virtual std::string returnSerializedString( ) const = 0;
        size_t getHeight() const{
            return height;
        }
    protected:
        size_t height;
        size_t lengthWalked;
        int movePenalty;

};
