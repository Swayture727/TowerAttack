#pragma once

#include "Unit.h"


class UnitFactory{
    public:
        Unit operator() (   size_t x, 
                            size_t y, 
                            char drawChar, 
                            int hp,
                            int dmg, 
                            int range,
                            int attackEff,
                            int moveEff,
                            int defEff,
                            int pathRestoration = 0) const;    

};
