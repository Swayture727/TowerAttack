#pragma once
#include <vector>
#include <optional>

#include "UnitMovementE.h"


class UnitFlyingMove : public UnitMovementE{
    public:
         UnitFlyingMove()
             :  UnitMovementE(1),
                flyingLength(0){}    
        UnitFlyingMove(const UnitFlyingMove & flyingMove) 
            :   flyingPath(flyingMove.flyingPath),
                flyingLength(flyingMove.flyingLength){
                }
        std::pair<size_t,size_t> walk( const UnitState & state) override;
        UnitFlyingMove * clone() const override;
        void postRoundReset() override;

        std::string returnSerializedString( ) const override;

        std::pair<size_t,size_t> setPath(   std::vector<std::pair<size_t,size_t>>::const_iterator walkingPathIn,
                                            size_t walkingLenghtIn,
                                            std::vector<std::pair<size_t,size_t>>::const_iterator flyingPathIn,
                                            size_t flyingLenghtIn, size_t pathRestoration ) override;
    private:
        std::optional<std::vector<std::pair<size_t,size_t>>::const_iterator> flyingPath;
        size_t flyingLength;
};
