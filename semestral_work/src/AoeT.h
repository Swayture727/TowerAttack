#pragma once

#include "TowerEffect.h"
#include "UnitStats.h"

/**
 * A tower that attacks all available units and then reloads for a while.
 * @see TowerEffect
 */
class AoeT : public TowerEffect{
    public:
        AoeT( int dmg )
            :   TowerEffect(dmg, 5),
                fired(0){}
//----------------------------------------------------------------------------------//
        bool affect(UnitStats & unit, size_t height) override;
//----------------------------------------------------------------------------------//
        int getSerializedVal() const override;
//----------------------------------------------------------------------------------//
        void postRoundReset() override;
//----------------------------------------------------------------------------------//
        AoeT * clone() const override;
    private:
        int fired;
};
