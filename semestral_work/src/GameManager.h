
#include <cstdlib>
#include <memory>
#include <filesystem>
#include <tuple>
#include <vector>

#include "ConfigManager.h"
#include "Tower.h"
#include "MenuManager.h"
#include "GameMap.h"

/**
 * A class that manages, running the game and loading/saving current game.
 */

class GameManager{
    public:
        GameManager();
        /** 
        * Starts the game. 
        */
        void run();
		
//----------------------------------------------------------------------------------//
        /**
         * Spawns all the units at the beginning of a round.
         */
        void spawnUnits();

//----------------------------------------------------------------------------------//
        /**
         * Spawns units, makes them attack towers if possible, also checks if a unit has reached exit.
         */
        void unitAction();
//----------------------------------------------------------------------------------//
        /**
         * Makes towers attack units. 
         */
        void towerAction();

//----------------------------------------------------------------------------------//
        /**
         * Places a tower. 
         */
        void placeTower();
//----------------------------------------------------------------------------------//
        /** 
         * Saves the current game.
         */
        void saveGame();

//----------------------------------------------------------------------------------//
        /** 
         * Loads saved game.
         */
        void loadGame();

//----------------------------------------------------------------------------------//
        /**
         * Starts the game itself, by showing the main menu. 
         */
        void mainGameLoop();

//----------------------------------------------------------------------------------//
        /**
         * Some units and towers need to be reset after each whole tick, that is the job of this method.
         */
        void postRoundReset();
//----------------------------------------------------------------------------------//
        /**
         * A wrapper that calls spawnUnits(), placeTower(), unitAction(), postTowerReset(). 
         * @see spawnUnits().
         * @see placeTower().
         * @see UnitAction().
         * @see PostTowerReset().
         */
        void turnActions();
//----------------------------------------------------------------------------------//
        /**
         * Displays current enemy base HP and remaining units that will be spawned.
         */
        void displayStats();

//----------------------------------------------------------------------------------//
    private:
        enum GameState {wonGame, lostGame, activeGame, quitGame};
        GameState state;
        /**
         * Amount of towers destroyed.
         */
        int score;
        int baseHP;
        int numberOfRounds;
        int unitsRemaining;

        bool evenTick;
        /**
         * Tracks the number of ticks, used to spawn a tower every 32 ticks.
         */
        int tickCounter;
        
        MenuManager menu;
               
        GameMap gameMap; 
        /**
         * Handles config loading.
         */
        ConfigManager configManager; 


        /**
         * All towers that will cycled through while placing towers.
         */
        std::vector<Tower> availableTowers;
        /**
         * Tracks last placed tower.
         */
        std::vector<Tower>::const_iterator lastSpawnedTower;

        /**
         * All units that will cycled through while spawning units.
         */
        std::vector<Unit> availableUnits;
        /**
         * Tracks last spawned unit.
         */
        std::vector<Unit>::const_iterator lastSpawnedUnit;
             

};
