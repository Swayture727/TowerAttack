

#include <memory>
#include <sstream>
#include <cctype>

#include "AntiAirT.h"
#include "AoeT.h"
#include "TowerFactory.h"


Tower TowerFactory::operator() (    char c,
                                    int hp,
                                    int dmg,
                                    int range,
                                    std::vector<size_t> effects) const{
    
    std::vector<std::unique_ptr<TowerEffect>> readyEffects;
    std::vector<bool> doesItAlreadyHaveThisEffect = std::vector<bool>(3,false); 
    for( size_t e : effects ){
        if( e < doesItAlreadyHaveThisEffect.size() &&
                !doesItAlreadyHaveThisEffect[e] ){
            switch(e){
                case(0):
                    readyEffects.emplace_back(new BasicAttackT(dmg));
                    break;
                case(1):
                    readyEffects.emplace_back(new AoeT(dmg));
                    break;
                case(2):
                    readyEffects.emplace_back(new SlowT());
                    break;
                case(3):
                    readyEffects.emplace_back(new AntiAirT(dmg));
                    break;
            }

        }
    }
    return Tower(0,0,c,hp,dmg,range,readyEffects);
}


Tower TowerFactory::createActiveTower(   int x, 
                                         int y, 
                                         char c, 
                                         int hp, 
                                         int dmg, 
                                         int range, 
                                         std::vector<size_t> effects) const{
    Tower tower = (*this)(c,hp,dmg,range,effects);
    tower.buildTowerOn({x,y});
    return tower;
}
