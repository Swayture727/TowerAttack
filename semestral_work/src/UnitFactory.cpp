
#include "UnitFactory.h"

#include "UnitTankDef.h"
#include "UnitGroundMovementE.h"
#include "UnitFlyingMove.h"
#include "UnitSniperAtt.h"
#include "UnitDefensiveE.h"

Unit UnitFactory::operator() (  size_t x, 
                                size_t y, 
                                char drawChar, 
                                int hp,
                                int dmg, 
                                int range,
                                int attackEff,
                                int moveEff,
                                int defEff,
                                int pathRestoration) const{
    UnitAttackE * attack;
    UnitMovementE * move;
    UnitDefensiveE * def;
    switch( attackEff ){
        case(0):
            attack = new UnitAttackE();
            break;
        case(1):
            attack = new UnitSniperAtt();
            break;
        default:
            attack = new UnitAttackE();
            break;
    }
    switch( moveEff ){
        case(0):
            move = new UnitGroundMovementE();
            break;
        case(1):
            move = new UnitFlyingMove();
            break;
        default:
            move = new UnitGroundMovementE();
            break;
    }
    switch( defEff ){
        case(0):
            def = new UnitDefensiveE();
            break;
        case(1):
            def = new UnitTankDef();
            break;
        default:
            def = new UnitDefensiveE();
            break;
    }
    return Unit(x, y, drawChar, hp, dmg, range, attack, move, def, pathRestoration);
}
