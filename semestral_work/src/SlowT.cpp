

#include "SlowT.h"

bool SlowT::affect(UnitStats & unit, size_t height) {
    unit.slowUnit();
    return false;
}

void SlowT::postRoundReset(){

}

SlowT * SlowT::clone() const{
    return new SlowT();
}

int SlowT::getSerializedVal() const{
    return 2; 
}
