#pragma once

#include <memory>
#include <iostream>
#include <queue>
#include <memory>

#include "Wall.h"
#include "Unit.h"
#include "Entrance.h"
#include "Exit.h"

/**
 * Contains the game map.
 */

class GameMap{
    public:
        GameMap() 
            :   width(0), 
                height(0),
                nextTowerWallIndex(0){}

        /**
         * Draws the map.
         */
        void drawMap() const; 

//----------------------------------------------------------------------------------//
        /**
         * A wrapper function that calls both the canFly and non flying version of findPaths().
         * @see findPaths().
         */

        void loadPaths();

//----------------------------------------------------------------------------------//
        /**
         * Prepares map for game, effectively calls loadPaths() and prepares walls for getNextTowerPlacement().
         * @see loadPaths().
         * @see getNextTowerPlacement().
         */
        void prepareMap();

//----------------------------------------------------------------------------------//
        /**
         * Gets next position for a tower to be placed.
         * @return coordinates for the next tower and a bool to signify if there is space left - the cords are valid.
         */
        std::tuple<size_t,size_t, bool> getNextTowerPlacement(); 

        size_t width;
        size_t height;
        std::vector<bool> gameBoard;

        std::vector<Wall> walls;
        std::vector<std::shared_ptr<Tower>> towers;
        std::vector<Unit> units;

        Entrance entrance;

        Exit exit;

        std::vector<std::pair<size_t,size_t>> flyingPath;

        std::vector<std::pair<size_t,size_t>> walkingPath;

    private:
        
        size_t nextTowerWallIndex;

        size_t manhattanDistanceFromExit( std::pair<size_t, size_t> a) const; 

        /**
         * Finds two shortest paths using BFS, first is for walking units (can't walk over walls)
         * and the second one is goes straight to exit.
         * @param gameMap a game map to search in.
         * @param straightPath if the search should or shouldn't expand over walls.
         */
        void findPaths(bool straightPath) ;

        /**
         * A helper method used in findPaths, expands given index.
         * @param index an index to expand.
         * @param tileIndexes stores indexes, that should get processed adds expanded indexes.
         * @param tileParents a vector that stores from what index the BFS got there, 
         * first int is mapping the first gameTile.
         * @param gameMap a game map to search in.
         * @param straightPath whether it can expand over walls.
         */
        void expand(size_t index, std::queue<int>& tileIndexes,
                    std::vector<int>& tileParents, bool straightPath = false) ;

        /**
         * Reconstructs path (finds indexes) of path from entrance to exit.
         * @param tileParents source of indexes.
         * @param gameMap source game map.
         * @param index exit index.
         */
        void reconstructPath(std::vector<int>& tileParents, size_t index, bool straightPath) ;


};
