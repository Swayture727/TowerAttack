

#include "UnitStats.h"
#include <string>


bool UnitStats::postRoundReset(){
    if( currentState != UnitState::dead ){
        currentState = UnitState::alive;
        return false;
    }
    return true;
}

void UnitStats::slowUnit(){
    currentState = UnitState::slowed;	
}

bool UnitStats::receiveDmg (int dmg){
    hp -= takeDamage->takeDmg(dmg);
    if( hp <= 0 ){
        currentState = UnitState::dead;
        return true;
    }
    return false;
}


UnitState UnitStats::getStatus() const{
    return currentState;
}

UnitStats & UnitStats::operator=(const UnitStats & stats){
    hp = stats.hp;
    currentState = stats.currentState;
    takeDamage = std::unique_ptr<UnitDefensiveE>(takeDamage->clone());
    return *this;
}


std::string UnitStats::returnSerializedString( ) const{
    return std::to_string(hp)  +"," + std::to_string(takeDamage->returnSerializedString());
}
