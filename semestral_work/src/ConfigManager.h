#pragma once

#include <filesystem>
#include <memory>
#include <vector>
#include <queue>
#include <memory>

#include "GameMap.h"
#include "GameTileMaker.h"

/**
 * Handles configs - loading, parsing, validating.
 */

class ConfigManager{
    public:
        ConfigManager();
        /** 
         * Loads given unit settings.
         * Loads and validates ../../examples/configFiles/unitConfig.txt, load default if it isn't able to parse the file.
         * @param cords coordinates of the map entrance - where unit should spawn.
         * @param filename used to specify a config file - used while loading a saved game.
         * @return The loaded units `from the file or default values.
         */
        std::shared_ptr<std::vector<Unit>> loadUnitSettings( std::pair<size_t, size_t> cords, std::string filename = "") const;
//----------------------------------------------------------------------------------//
        /** 
         * Loads given tower settings.
         * Loads and validates ../../examples/configFiles/towerConfig.txt.
         * @exception std::logic_error could not parse or find the given config file.
         * @param filename used to specifiy a config file - used while loading a saved game. 
         * @param active whether or not the loaded towers should be treated as active/in combat or not.
         * @return The loaded towers from the file or default values.
         * */
        std::shared_ptr<std::vector<std::shared_ptr<Tower>>> loadTowerSettings(std::string filename = "", bool active = false) const;
//----------------------------------------------------------------------------------//
        /**
         * Used to look for a file in the config folder.
         * @param name name of the file to look for.
         * @param subDirectory used to specify subdirectory in config folder.
         * @return path to a potential config/map file.
         */
        std::filesystem::path findFileWithName( std::string name, std::string subdirectory = "" ) const;
//----------------------------------------------------------------------------------//
        /** 
         * Parses a map file and if correct returns it as a vector.
         * @exception std::logic_error could not parse the given map file.
         * @return Loaded map.
         */
        GameMap loadMap( std::string name = "map.txt", std::string subDirectory = "") ;
//----------------------------------------------------------------------------------//
        /**
         * Specifies path for saved map, effectively just calls loadMap() with a specific file. 
         * @see loadMap().
         * @return Loaded map.
         */
        std::optional<GameMap> getSavedMap() ;



//----------------------------------------------------------------------------------//
    private:
        GameTileMaker maker;
        std::string pathToConfigFolder;
        std::string unitConfigName;
        std::string towerConfigName;
}; 
