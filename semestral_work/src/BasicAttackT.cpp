#include "BasicAttackT.h"

bool BasicAttackT::affect(UnitStats & unit, size_t height) {
    if( fired <= 0 ){
        fired = 2;
        if( height == 0 ){
           return unit.receiveDmg( dmg ); 
        } 
        else{
           return unit.receiveDmg( dmg / 2 ); 
        }
    }
    return false;
}
//----------------------------------------------------------------------------------//
BasicAttackT * BasicAttackT::clone() const{
    return new BasicAttackT( dmg );
}
//----------------------------------------------------------------------------------//
void BasicAttackT::postRoundReset() {
    fired -= 1;
}
//----------------------------------------------------------------------------------//
int BasicAttackT::getSerializedVal() const{
    return 0;
}
