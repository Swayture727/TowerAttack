
#include "GameSprite.h"

/**
 * Stores map exit.
 */
class Exit{
    public:
        Exit(size_t x = 1,size_t y = 1)
            : sprite(x,y, '<'){}

        std::pair<size_t,size_t> getCords() const;
    private:
        GameSprite sprite;
};
