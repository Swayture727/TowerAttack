#include <cstdio>
#include <exception>
#include <filesystem>
#include <iostream>
#include <fstream>
#include <cctype>
#include <memory>

#include "ConfigManager.h"
#include "GameMap.h"
#include "TowerFactory.h"
#include "UnitFactory.h"

#define MAX_WIDTH  2500 
#define MAX_HEIGHT 2500 


ConfigManager::ConfigManager()
    :   pathToConfigFolder("./examples"),
        unitConfigName("unitConfig.txt"),
        towerConfigName("towerConfig.txt"){}


//----------------------------------------------------------------------------------//
std::optional<GameMap> ConfigManager::getSavedMap(){
    try{
       return loadMap("SavedMap.txt", "/configFiles"); 
    }
    catch(std::exception & e){
        return std::optional<GameMap>();
    }
}

//----------------------------------------------------------------------------------//
std::shared_ptr<std::vector<std::shared_ptr<Tower>>> ConfigManager::loadTowerSettings(std::string path, bool active) const{ 
    std::shared_ptr<std::vector<std::shared_ptr<Tower>>> output = std::make_shared<std::vector<std::shared_ptr<Tower>>>();
    std::filesystem::path configPath = findFileWithName(path == "" ? towerConfigName : path, "/configFiles" );
    TowerFactory factory;
    bool invalidConfig = configPath.filename() == "null" ? true : false;

    char drawChar = 'T';
    int x = -1;
    int y = -1;
    int hp = -1;
    int dmg = -1;
    int range = -1;
    int effect = -1;
    std::vector<size_t> effects;


    char c = ' ';

    std::ifstream stream = std::ifstream(configPath,std::ios::in);

    auto lookForDelimiter = [&](){ stream >> c; invalidConfig = ( c != ',' || stream.bad()); };
    auto checkStream = [&](int val){ invalidConfig = (!(bool)stream || val == -1);};

    while( !invalidConfig && stream.good() ){
        stream >> drawChar;
        if( !stream )
            break;
        if( !std::isalnum(drawChar) )
            invalidConfig = true;

        lookForDelimiter();

        stream >> x;
        checkStream(hp);
        lookForDelimiter();

        stream >> y;
        checkStream(hp);
        lookForDelimiter();

        stream >> hp;
        checkStream(hp);
        lookForDelimiter();

        stream >> dmg; 
        checkStream(dmg);
        lookForDelimiter();


        stream >> range; 
        checkStream(range);
        lookForDelimiter();

        while( stream && !invalidConfig ){
            stream >> effect;
            if( effect == -1 ){
                stream.clear();
                break;
            }
            effects.push_back(effect);
            lookForDelimiter();
            if( c == ',' && stream.eof() )
                invalidConfig = false;
            effect = -1;
        }
        invalidConfig = effects.size() == 0;
        if( !invalidConfig ){
            if( active )
                output->push_back(std::make_shared<Tower>(factory.createActiveTower(x,y,drawChar,hp,dmg,range,effects)));
            else{ 
                output->push_back(std::make_shared<Tower>(factory(drawChar,hp,dmg,range,effects)));
            }
        }
        hp = -1;
        dmg = -1;
        range = -1;
        effect = -1;
    }
    if( (invalidConfig || output->size() == 0) && !active ){
        std::string dummy;
        std::cout << "Couldnt load the tower config, loading defaults" << std::endl;
        std::getline(std::cin, dummy);
        output->clear();
        output->emplace_back(std::make_shared<Tower>(factory('O',100,100,5,{0})));
        output->emplace_back(std::make_shared<Tower>(factory('T',100,10,20,{2})));
        return output;
    }

    std::string dummy;
    std::cout  << "Managed to read the tower config."  <<std::endl;
    std::getline(std::cin, dummy);
    return output;
}
//----------------------------------------------------------------------------------//
std::shared_ptr<std::vector<Unit>>  ConfigManager::loadUnitSettings(std::pair<size_t,size_t> entranceCords, std::string fileName) const{ 
    UnitFactory factory;
    std::shared_ptr<std::vector<Unit>> output = std::make_shared<std::vector<Unit>>(std::vector<Unit>());
    std::filesystem::path configPath = findFileWithName(fileName == "" ? unitConfigName : fileName, "/configFiles" );
    bool invalidConfig = configPath.filename() == "null" ? true : false;

    char drawChar = 'I';
    int hp, dmg, range, attack, move, def, length;


    char c = ' ';

    std::ifstream stream = std::ifstream(configPath,std::ios::in);

    auto lookForDelimiter = [&](){ stream >> c; invalidConfig = ( c != ',' || !stream); };
    auto checkStream = [&](){ invalidConfig = !(bool)stream;};

    while( !invalidConfig && stream ){
        stream >> drawChar;
        if( !stream )
            break;
        if( !std::isalnum(drawChar) )
            invalidConfig = true;

        lookForDelimiter();

        stream >> dmg;
        checkStream();
            lookForDelimiter();

        stream >> range; 
        checkStream();
        lookForDelimiter();


        stream >> hp; 
        checkStream();
        lookForDelimiter();

        stream >> def; 
        checkStream();
        lookForDelimiter();

        stream >> attack; 
        checkStream();
        lookForDelimiter();

        stream >> move; 
        checkStream();
        lookForDelimiter();

        stream >> length;
        if( !invalidConfig ){
            output->push_back(factory(entranceCords.first, entranceCords.second,drawChar,hp,dmg,range,attack,move,def, length));
        }
    }

    if( invalidConfig || output->size() == 0 ){
        std::string dummy;
        std::cout << "Couldnt load the unit config, loading defaults" << std::endl;
        output->clear();
        output->push_back(factory(entranceCords.first, entranceCords.second,'@',50,2,5,0,0,0,0));
        output->push_back(factory(entranceCords.first, entranceCords.second,'%',50,2,5,0,1,0,0));
        return output;
    }

    std::cout  << "Managed to read the unit config."  <<std::endl;
    return output;
}
//----------------------------------------------------------------------------------//
std::filesystem::path ConfigManager::findFileWithName( std::string name, std::string subDirectory) const{
    auto dirIter = std::filesystem::directory_iterator{pathToConfigFolder + subDirectory};

    for( auto & entry : dirIter ){
        if( entry.is_regular_file() && entry.path().filename() == name){
            return entry.path();	
        }
    }
    return std::filesystem::path{"/dev/null"};
}
//----------------------------------------------------------------------------------------//
GameMap ConfigManager::loadMap( std::string name, std::string subDirectory ) {
    std::filesystem::path mapPath = findFileWithName(name, subDirectory);
    size_t entranceX = 0;
    size_t entranceY = 0;

    size_t exitX = 0;
    size_t exitY = 0;
    GameMap returnVal;
    if( mapPath.filename() == "null" ){
        //TODO add a suitable default if no map is found
        throw std::logic_error("Can't find a map!");
        return GameMap();
    } 
    std::ifstream stream = std::ifstream(mapPath,std::ios::in);
    char c = stream.get();
    while( !stream.eof() && c != '\n' && c == '#'){
        returnVal.walls.push_back(Wall(returnVal.width,0));
        returnVal.gameBoard.emplace_back(maker.createGameTile(c));
        returnVal.width += 1;
        c = stream.get();
    } 
    returnVal.height += 1;
    if( c != '\n' ){
        throw std::logic_error("The first row must only consist of '#' - wall characters!");
        } 
    else if( returnVal.width > MAX_WIDTH){	
        throw std::logic_error( std::string("The given map is too wide, the max width is ")
                                + std::string(std::to_string(MAX_WIDTH)) + "!");
    } 
    bool mightBeLast = false;
    c = stream.get();
    while( !stream.eof() ){
        size_t lineWidth = 0;
        if( mightBeLast )
            throw std::logic_error("there can't be a row full of # inside the map!");
        mightBeLast = true;
        if ( c !=  '#' && c != '>')
            throw std::logic_error(std::string(	"The first collumn(apart from first and last row)"
                                                "must only consist of '#' - wall and"
                                                "'>' - entrance characters!I got: '")
					        + c + std::string("'"));	

        if( c == '>' ){
            if( entranceX != entranceY )
                throw std::logic_error("There can be only one entrance!"); 
            entranceY = returnVal.height;
        }
        char secondToLast;
        while( c != '\n' && !stream.eof() ){	
            if( c != '#' )
                mightBeLast = false;
            else{
                returnVal.walls.push_back(Wall(lineWidth%returnVal.width,returnVal.height));
	    }
            returnVal.gameBoard.emplace_back(maker.createGameTile(c));
            secondToLast = c;
            c = stream.get();
            lineWidth += 1;
	}
        returnVal.height += 1;
	
        if( lineWidth != returnVal.width ){	
            throw std::logic_error( std::string("All rows must have the same length ") + 
                                    std::string(std::to_string(lineWidth)));
	}
        else if( secondToLast != '#' && secondToLast != '<'  ){
            throw std::logic_error(std::string( "The last collumn (apart from first and last row)" 
                                                "must only consist of '#' - wall"
                                                "and '<' - exit characters! I got: '") 
                                                + secondToLast + std::string("'") );
	}	
        if( secondToLast == '<' ){
            if( exitX != exitY )
                throw std::logic_error("There can be only one exit!"); 
            exitY = returnVal.height;
        }
        c = stream.get();
        }
    if( !mightBeLast ){	
        throw std::logic_error("The last row must only consist of '#' - wall characters!");
        }
    else if ( returnVal.height > MAX_HEIGHT )
        throw std::logic_error( std::string("The given map is too high, the max height is")
                                + std::string(std::to_string(MAX_HEIGHT)) + "!");

    returnVal.exit = Exit(returnVal.width -1, exitY-1);
    returnVal.entrance = Entrance(entranceY);

    returnVal.prepareMap();
    return returnVal;
}
