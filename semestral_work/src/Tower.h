#pragma once

#include <memory>
#include <vector>

#include "AntiAirT.h"
#include "AoeT.h"
#include "BasicAttackT.h"
#include "SlowT.h"
#include "TowerEffect.h"
#include "GameSprite.h"

class Tower{
    public:
        void place();
        void collaps();
        Tower(  size_t x, 
                size_t y, 
                char c, 
                int hp, 
                int dmg,
                int range,
                std::vector<std::unique_ptr<TowerEffect>> & effectsIn);
        Tower( const Tower & tower );
        Tower & operator =( const Tower & tower );
        
        void drawTower() const;
        std::pair<size_t,size_t> getCords() const;
        bool affect( UnitStats & stats, std::tuple<size_t,size_t,size_t> unitCords ) const;
         
        void postRoundReset();

        bool takeDamage( int dmg );

        int getValue() const;

        char getDrawChar() const;

        void buildTowerOn( std::pair<size_t, size_t> cords );

        std::string returnSerializedString( ) const;
    private:
        int hp;
        int dmg;
        size_t range;
        GameSprite sprite;
        int towerValue;
        std::vector<std::unique_ptr<TowerEffect>> effects;

};
