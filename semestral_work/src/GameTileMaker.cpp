
#include <stdexcept>
#include <string>

#include "GameTileMaker.h" 


bool GameTileMaker::createGameTile(char identifier){
    auto mapIter = possibleTiles.find(identifier);
    if( mapIter == possibleTiles.end() ){
        //TODO: change exception msg
        throw(std::logic_error(
                    (std::string("Can't parse '") 
                    + identifier + 
                    std::string("' as a gametile! Only '#',' ', '<', '>' are allowed"))));
    }
    return mapIter->second();
}
